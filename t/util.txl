ID   DCTQ_RHOSH  PRELIMINARY;      PRT;   227 AA.
AC   Q9LBD9;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   Putative small C4-dicarboxylate integral membrane transport protein.
GN   DCTQ.
OS   Rhodobacter sphaeroides (Rhodopseudomonas sphaeroides).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhodobacter group;
OC   Rhodobacter.
OX   NCBI_TaxID=1063;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=4P1;
** nothing has been published
RA   Omrani M.D.;
RL   Submitted (MAY-1997) to the EMBL/GenBank/DDBJ databases.
CC   -!- FUNCTION: REQUIRED FOR C4-DICARBOXYLATE TRANSPORT (BY SIMILARITY).
CC   -!- SUBCELLULAR LOCATION: INTEGRAL MEMBRANE PROTEIN (BY SIMILARITY).
 **this miscellaneous CC line should be deleted
CC   -!- MISCELLANEOUS: SHOWS POOR SPECIFICITY.
DR   EMBL; AF005842; AAF72734.1; -.
KW   Transmembrane; Transport.
FT   DOMAIN        1     10       CYTOPLASMIC (BY SIMILARITY).
FT   TRANSMEM     11     29       BY SIMILARITY.
FT   DOMAIN       30     68       PERIPLASMIC (BY SIMILARITY).
FT   TRANSMEM     69     91       BY SIMILARITY.
FT   DOMAIN       92    112       CYTOPLASMIC (BY SIMILARITY).
FT   TRANSMEM    113    133       BY SIMILARITY.
FT   DOMAIN      134    152       PERIPLASMIC (BY SIMILARITY).
FT   TRANSMEM    153    173       BY SIMILARITY.
FT   DOMAIN      174    227       CYTOPLASMIC (BY SIMILARITY).
**
**   #################     SOURCE SECTION     ##################
**   Rhodobacter sphaeroides C4-dicarboxylate binding-protein precursor (dctP),
**   small integral membrane transport protein (dctQ) and large integral
**   membrane transport protein (dctM) genes, complete cds.
**   source          1..3718
**                   /db_xref="taxon:1063"
**                   /organism="Rhodobacter sphaeroides"
**                   /strain="4P1"
**   CDS             1214..1897
**                   /codon_start=1
**                   /note="DctQ; contains four putative transmembrane helices"
**                   /transl_table=11
**                   /gene="dctQ"
**                   /product="small integral membrane transport protein"
**                   /protein_id="AAF72734.1"
**   CDS_IN_EMBL_ENTRY 3
**   02-JUN-2000 (Rel. 63, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_RHOSH
**ZZ CREATED AND FINISHED BY MICHELE.
**ZZ CURATED.
SQ   SEQUENCE   227 AA;  24991 MW;  2208AD920C1230DA CRC64;
     MMRLLDRLEE TLIASLIAAA TGLIFVSVVQ RYSLGLLADG VAFFRGHDMP ELSAMMRSAY
     LGLREFNLVW AQELCIILFV WMAKFGAAYG VRTGIHVGID VLINKLDERK RGFFILLGLG
     AGALFTGIIA TLGGNFVWHM AQTSAISPDL ELPMWLVYLA IPLGSALMCF RFLQVAVIFA
     RTGELAHHDH GHVEGVDTED EGIDVLGSTF LKSPLTPRDL VEKPKDE
//
ID   DCTQ_WOLSU  PRELIMINARY;      PRT;   170 AA.
AC   Q9ZEJ3;
DT   01-MAY-1999 (TrEMBLrel. 10, Created)
DT   01-MAY-1999 (TrEMBLrel. 10, Last sequence update)
DT   01-MAY-1999 (TrEMBLrel. 10, Last annotation update)
 DE   Small C4-dicarboxylate integral membrane transport protein.
 ** unsure gene name,
 ** will be deleted
GN   DCTQ.
OS   Wolinella succinogenes.
OC   Bacteria; Proteobacteria; epsilon subdivision; Helicobacter group;
OC   Wolinella.
OX   NCBI_TaxID=844;
RN   [1]
RP   SEQUENCE FROM N.A.
 RC   STRAIN=DSMZ 1740;
 RX   MEDLINE=20461222; PubMed=11004174;
 RA   Ullmann R., Gross R., Simon J., Unden G., Kroeger A.;
 RT   "Transport of C(4)-dicarboxylates in Wolinella succinogenes.";
 RL   J. Bacteriol. 182:5757-5764(2000).
 CC   -!- FUNCTION: INVOLVED IN THE ELECTROGENIC UNIPORT OF C4-
 CC       DICARBOXYLATES.
**   or should this be in similarity?, do things in similarity have to have
**   a dr line?
 CC   -!- PATHWAY: BELONGS TO THE TRIPARTITE ATP-INDEPENDENT PERIPLASMIC
 CC       (TRAP) TRANSPORTER FAMILY
DR   EMBL; AJ132740; CAA10757.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Wolinella succinogenes dctP, dctQ and dctM genes, and partial orfN
**   source          1..3309
**                   /organism="Wolinella succinogenes"
**                   /db_xref="taxon:844"
**   CDS             1086..1598
**                   /db_xref="PID:e1375211"
**                   /transl_table=11
**                   /gene="dctQ"
**                   /product="small integral C4-dicarboxylate membrane
**                   transport protein, putative"
**                   /protein_id="CAA10757.1"
**   CDS_IN_EMBL_ENTRY 4
**   04-FEB-1999 (Rel. 58, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_WOLSU
**ZZ CREATED AND FINISHED BY MICHELE.
SQ   SEQUENCE   170 AA;  18753 MW;  4CCF9F77E2F85C20 CRC64;
     MSKFFEILDL GIAAMNKSIA VIGISTGVIL AFINVVLRYF FDSGLTWAGE TINYLFIWSA
     LFGAAYGFKK GIHIAVTILV ERFPPLLAKA SLMIASLISL IFLLFIAYFG LHYVLLVKDM
     GFMSVDLGIP QWIPMVVIPV AFFAASYRVG EKIYEISKQP ADTVVKSAGR
//
ID   DCTQ_RHOCA  PRELIMINARY;      PRT;   227 AA.
AC   O07837;
DT   01-JUL-1997 (TrEMBLrel. 04, Created)
DT   01-JUL-1997 (TrEMBLrel. 04, Last sequence update)
DT   01-NOV-1998 (TrEMBLrel. 08, Last annotation update)
 DE   Small C4-dicarboxylate integral membrane transport protein.
GN   DCTQ.
OS   Rhodobacter capsulatus (Rhodopseudomonas capsulata).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhodobacter group;
OC   Rhodobacter.
OX   NCBI_TaxID=1061;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=B11;
RX   MEDLINE=92236423; PubMed=1809844;
RA   Shaw J.G., Hamblin M.J., Kelly D.J.;
RT   "Purification, characterization and nucleotide sequence of the
RT   periplasmic C4-dicarboxylate-binding protein (DctP) from Rhodobacter
RT   capsulatus.";
RL   Mol. Microbiol. 5:3055-3062(1991).
RN   [2]
RP   SEQUENCE FROM N.A.
** I see SB10003 / St Louis as the strain from table 1, not B10.
RC   STRAIN=B11;
RX   MEDLINE=97431499; PubMed=9287004;
RA   Forward J.A., Behrendt M.C., Wyborn N.R., Cross R., Kelly D.J.;
RT   "TRAP transporters: a new family of periplasmic solute transport
RT   systems encoded by the dctPQM genes of Rhodobacter capsulatus and by
RT   homologs in diverse gram-negative bacteria.";
RL   J. Bacteriol. 179:5482-5493(1997).
 RN   [3]
 RP   DISCUSSION OF SEQUENCE.
 RX   MEDLINE=20090467; PubMed=10627041;
 RA   Rabus R., Jack D.L., Kelly D.J., Saier M.H. Jr;
 RT   "TRAP transporters: an ancient family of extracytoplasmic
 RT   solute-receptor-dependent secondary active transporters.";
 RL   Microbiology 145:3431-3445(1999).
 RN   [4]
 RP   TOPOLOGY.
 RC   STRAIN=B10;
 RX   PubMed=11150659;
 RA   Wyborn N.R., Alderson J., Andrews S.C., Kelly D.J.;
 RT   "Topological analysis of DctQ, the small integral membrane protein of
 RT   the C4-dicarboxylate TRAP transporter of Rhodobacter capsulatus.";
 RL   FEMS Microbiol. Lett. 194:13-17(2001).
 CC   -!- FUNCTION: REQUIRED FOR C4-DICARBOXYLATE TRANSPORT (table 2,ref2).
**   we could also say that transport is thought to use the membrane
**   potential, in that case we might put it into function. Note that in
**   ref 4 the use of an electrochemical gradient for energy is now sure
**   and is cited in the intro.
 CC   -!- ENZYME REGULATION: TRANSPORT IS SENSITIVE TO MEMBRANE POTENTIAL
 CC       UNCOUPLERS AND VANADATE INSENSITIVE (ref2 figs 6&7).
** inner membrane upon expression in ecoli, but can we extrapolate to Rhoca?
 CC   -!- SUBCELLULAR LOCATION: INTEGRAL MEMBRANE PROTEIN (ref2 fig4).
DR   EMBL; X63974; CAA45386.1; -.
DR   PROSITE; PS12345; TEST_DOMAIN; 1.
**   PROSITE; PS12346; FALSE_DOMAIN; FALSE_POS.
 KW   Transmembrane; Transport.
 FT   DOMAIN        1     10       CYTOPLASMIC.
 FT   TRANSMEM     11     29
 FT   DOMAIN       30     68       PERIPLASMIC.
 FT   TRANSMEM     69     91
 FT   DOMAIN       92    112       CYTOPLASMIC.
 FT   TRANSMEM    113    133
 FT   DOMAIN      134    152       PERIPLASMIC.
 FT   TRANSMEM    153    173
 FT   DOMAIN      174    227       CYTOPLASMIC.
** transmembrane regions are unsure
**
**   #################     SOURCE SECTION     ##################
**   R capsulatus dctP gene for C4-dicarboxylase binding protein
**   source          1..4500
**                   /organism="Rhodobacter capsulatus"
**                   /strain="B10"
**                   /clone_lib="Cosmid, pLAFR1"
**                   /clone="pDCT200, pDCT205"
**                   /chromosome="1"
**   CDS             1084..1767
**                   /gene="dctQ"
**                   /product="small integral membrane transport protein"
**                   /db_xref="PID:e324693"
**   CDS_2_OUT_OF_5
**   26-JUN-1997 (Rel. 52, Last updated, Version 20)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_RHOCA
SQ   SEQUENCE   227 AA;  24763 MW;  0DE9D59DE5450F99 CRC64;
     MLRILDRAEE VLIAALIATA TVLIFVSVTH RFTLGFVADF VGFFRGHGMT GAAAAAKSLY
     TTLRGINLVW AQELCIILFV WMAKFGAAYG VRTGIHVGID VLINRLDAPK RRFFILLGLG
     AGALFTGIIA TLGANFVLHM YHASSTSPDL ELPMWLVYLA IPMGSSLMCF RFLQVAFGFA
     RTGELPHHDH GHVDGVDTEN EGIDAEGDVL LHSPLTPRDL VEKPKDN
//
